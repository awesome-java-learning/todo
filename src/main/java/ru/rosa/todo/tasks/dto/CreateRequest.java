package ru.rosa.todo.tasks.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class CreateRequest {
    @NotNull(message = "Имя задачи не задано")
    @Size(min = 1, message = "Слишком короткое имя задачи")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CreateRequest{" +
                "name='" + name + '\'' +
                ", completed=" + completed +
                ", creationDate=" + creationDate +
                ", completionDate=" + completionDate +
                '}';
    }

    public CreateRequest() {
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    private Boolean completed;
    private Date creationDate;
    private Date completionDate;
}
