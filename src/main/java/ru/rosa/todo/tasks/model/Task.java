package ru.rosa.todo.tasks.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Boolean completed;

    @Column(nullable = false)
    private Date creationDate;

    @Column(nullable = false)
    private Date completionDate;

    public Task() {
    }

    public Task(Integer id, String name, boolean completed) {
        this.id = id;
        this.name = name;
        this.completed = completed;
        this.creationDate = new Date();
    }

    public Task(Task t) {
        this.id = t.getId();
        this.name = t.getName();
        this.completed = t.getCompleted();
        this.creationDate = t.getCreationDate();
        this.completionDate = t.getCompletionDate();
    }

    public void copyTo(Task another) {
        this.id = another.getId();
        this.name = another.getName();
        this.completed = another.getCompleted();
        this.creationDate = another.getCreationDate();
        this.completionDate = another.getCompletionDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", completed=" + completed +
                ", creationDate=" + creationDate +
                ", completionDate=" + completionDate +
                '}';
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }
}
