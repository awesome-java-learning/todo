package ru.rosa.todo.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.rosa.todo.tasks.dto.CreateRequest;
import ru.rosa.todo.tasks.dto.SearchRequest;
import ru.rosa.todo.tasks.model.Task;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
@RequestMapping(path = "/tasks", produces = APPLICATION_JSON_VALUE)
public class TaskRest {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskRest.class);

    @Autowired
    private TaskService taskService;

    @Autowired
    private MappingService mapperService;

    @GetMapping("/echo") // GET /tasks/echo
    public String echo() {
        return "Hello, world!";
    }

    @GetMapping
    public List<Task> searchTasks(@RequestParam(name = "name", required = false) String name,
                                  @RequestParam(name = "completed", required = false) Boolean completed) {
        SearchRequest req = new SearchRequest();
        if (!StringUtils.isEmpty(name)) {
            req.setName(name);
        }
        if (completed != null) {
            req.setCompleted(completed);
        }
        List<Task> tasks = taskService.search(req);
        return tasks;
    }

    @PostMapping(produces = TEXT_PLAIN_VALUE, consumes = APPLICATION_JSON_VALUE)
    public String create(@RequestBody @Validated CreateRequest createRequest) {
        LOGGER.info("CreateReq: {}", createRequest);
        Integer id = taskService.create(createRequest);
        return id.toString();
    }

    @PutMapping(path = "{id}", consumes = APPLICATION_JSON_VALUE)
    public Task update(@RequestBody Task task) {
        LOGGER.info("updateReq: {}", task);
        Task result = taskService.update(task);
        return new Task(result); // https://www.baeldung.com/jackson-jsonmappingexception
    }

    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable("id") Integer id) {
        LOGGER.info("DeleteReq: {}", id);
        taskService.delete(id);
        return;
    }

    @GetMapping(path = "{id}")
    public Task get(@PathVariable("id") Integer id) {
        LOGGER.info("GetReq: {}", id);
        return new Task(taskService.get(id));
    }
}
