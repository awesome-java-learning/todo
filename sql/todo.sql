set session authorization postgres;

drop table if exists task;
create table task
(
    id              serial PRIMARY KEY, -- int
    name            varchar(2000) not null,
    completed       boolean       not null,
    creation_date   timestamp     not null,
    completion_date timestamp
);

insert into task (name, completed, creation_date)
values ('first', true, '2017-03-14');
insert into task (name, completed, creation_date)
values ('second', false, '2017-03-14');

drop user if exists todoapp;
create user todoapp;
alter role todoapp WITH PASSWORD 'todoapp';
GRANT CONNECT ON DATABASE todo TO todoapp;
GRANT USAGE ON SCHEMA public TO todoapp;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO todoapp;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO todoapp;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO todoapp;

set session authorization todoapp;
select *
from task
limit 10;