package ru.rosa.todo.tasks;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.springframework.stereotype.Service;

@Service
public class MappingService {
    Mapper mapper = DozerBeanMapperBuilder.buildDefault();
}
