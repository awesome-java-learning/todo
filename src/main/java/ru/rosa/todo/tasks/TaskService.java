package ru.rosa.todo.tasks;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.rosa.todo.tasks.dto.CreateRequest;
import ru.rosa.todo.tasks.dto.SearchRequest;
import ru.rosa.todo.tasks.model.Task;

import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TaskService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskService.class);
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MappingService mapperService;

    public List<Task> search(SearchRequest searchRequest) {
        LOGGER.debug("Search params: {}", searchRequest);
        Task example = new Task();

//        example.setCompleted(searchRequest.getCompleted());
////        example.setName(searchRequest.getName());

        LOGGER.warn("searchRequest: {}", searchRequest);
        mapperService.mapper.map(searchRequest, example);

        LOGGER.info("example: {}", example);

        List<Task> result = taskRepository.findAll(Example.of(example));
        return result;
    }

    public Integer create(CreateRequest createRequest) {
        LOGGER.debug("Create params: {}", createRequest);
        Task task = new Task(null, createRequest.getName(), false);
        task = taskRepository.save(task);
        return task.getId();
    }

    public Task get(Integer id) {
        return taskRepository.getOne(id);
    }

    public Task update(Task task) {
        Task taskDb = taskRepository.getOne(task.getId());
        taskDb.copyTo(task);
        return taskDb;
    }

    public void delete(Integer id) {
        taskRepository.deleteById(id);
    }
}
