package ru.rosa.todo.tasks.dto;

import javax.persistence.Column;
import java.util.Date;

public class SearchRequest {
    private String name;
    private Boolean completed;
    private Date creationDate;
    private Date completionDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SearchRequest() {
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "SearchRequest{" +
                "name='" + name + '\'' +
                ", completed=" + completed +
                ", creationDate=" + creationDate +
                ", completionDate=" + completionDate +
                '}';
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }
}
